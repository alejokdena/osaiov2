<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Osaio</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <link href="/css/index.css" rel="stylesheet">
</head>
<body>
  <!--header Menu-->
  <div class="container">
    <nav class="navbar navbar-expand-lg bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">Osaio</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/formulario">Formulario</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/">Home</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  <!--body contenido principal-->
    
    <div class="row">
      <div class="col">
        <p>
            Te ofrecemos un sofware sencillo, práctico y fácil de usar,este no será un dolor de cabeza.Creado para microempresarios, con la finalidad de mejorar su análisis financiero y la organización de la productividad de su inventario, proponemos un sistema de datos basado en el producto, llevado a cabo a partir de entrevistas realizadas a empleados y empleadores como usted, del negocio a la realidad, de la tienda de la esquina a su computadora.
        </p>      
        <p>
            Puedes tener diversas funcionalidades para administrar tu negocio desde una misma herramienta, usable desde tú dispositivo móvil, computadora de escritoio o portátil, no requiere de equipos con alta tecnología ni conocimientos en desarrollo.
        </p>
      </div>
    </div>
      <div class="row">
        <div class="col order-1">
          <h3 class="titles">REGISTRO DE INVENTARIO</h3>
          <img src="../img/inventario.jpg" class="img-fluid" alt="Inventario">
          <p> El sofware le permitirá registrar los productos con su cantidad, nombre, precio real y precio a la venta. </p>
        </div>
        <div class="col ">
          <h3 class="titles">INFORME DE VENTAS</h3>
          <p> Informe semanal o mensual comparativo del valor de las ventas reales y del valor de las ventas al público, haga el cálculo y sepa realmente cuánta es su ganancia</p>
          <img src="../img/informe-ventas.jpg" class="img-fluid" alt="Informe de ventas">
        </div>
        <div class="col">
          <h3 class="titles">REGISTRO DE VENTAS</h3>
          <img src="../img/registro-ventas.jpg" class="img-fluid" alt="Registro de ventas">
          <p> Venda tranquilamente, reciba el pago y el sistema le indicará el cambio que debe entregar, el saldo a favor e irá compilando el inventario</p>
        </div>
      </div>
      <div class="row">
        <div class="card border-light mb-12">
          <div class="card-header">Osaio</div>
          <div class="card-body">
            <h5 class="card-title">Sede principal</h5>
            <p class="card-text">
              <ul>
                <li>Av. Carrera 89 # 127c - 09, Torre 9 - Código postal 111131</li>
                <li>Atención presencial de lunes a viernes de 7.00 am a 4.30 pm</li>
                <li>Línea de atención al usuario: (57) 3213704606</li>
                <li>Notificaciones: <a href="mailto:dcacosta73@misena.edu.co">dcacosta73@misena.edu.co</a></li>
              </ul>
            </p>
          </div>
          <div class="card-footer bg-transparent border-success">@Copyright 2022</div>
        </div>
      </div>
    </div>
</body>
</html>