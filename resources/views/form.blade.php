<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>formulario Osaio</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <link href="/css/index.css" rel="stylesheet">
    <script src="/js/index.js"></script>
</head>
<body>
    <div class="container">
      <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Osaio</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="/formulario">Formulario</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div class="row">
          <div class="col">
            @if (session('success'))
                <div class="alert alert-success">
                        <stron>Gracias! </strong>{{session('success')}}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <strong>Error! </strong> {{session('error')}}
                </div>
            @endif
              <form method="POST" action="{{ route('save-contact') }}">
                  <div class="mb-3">
                    <label for="name" class="form-label">Nombre</label>
                    <input type="text" name="nombre" class="form-control" id="name">
                  </div>
                  <div class="mb-3">
                    <label for="email" class="form-label">email</label>
                    <input type="email" name="correo" class="form-control" id="email">
                  </div>
                  <div class="mb-3">
                      <label for="Cuentanos" class="form-label">Cuéntanos en qué podemos ayudarte</label>
                      <textarea class="form-control" name="descripcion" id="Cuentanos" rows="3"></textarea>
                  </div>
                  <div class="mb-3 form-check">
                    <input type="checkbox" class="form-check-input" id="Whatsapp">
                    <label class="form-check-label" for="Whatsapp">¿Quieres ser contactado vía whatsapp?</label>
                  </div>
                  <div class="mb-3" id="divPhone">
                    <label for="Phone" class="form-label">¿Cúal es su número de whatsapp?</label>
                    <input type="tel" name="celular" class="form-control" id="Phone">
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
      <div class="row">
        <div class="card border-light mb-12">
          <div class="card-header">Osaio</div>
          <div class="card-body">
            <h5 class="card-title">Sede principal</h5>
            <p class="card-text">
              <ul>
                <li>Av. Carrera 89 # 127c - 09, Torre 9 - Código postal 111131</li>
                <li>Atención presencial de lunes a viernes de 7.00 am a 4.30 pm</li>
                <li>Línea de atención al usuario: (57) 3213704606</li>
                <li>Notificaciones: <a href="mailto:dcacosta73@misena.edu.co">dcacosta73@misena.edu.co</a></li>
              </ul>
            </p>
          </div>
          <div class="card-footer bg-transparent border-success">@Copyright 2022</div>
        </div>
      </div>
    </div> 
</body>
</html>