<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Contact;
use Illuminate\Support\Facades\Input;
use Session;

class MainController extends Controller
{
    public function getContacts()
    {
        return Contact::all();
    }
    public function insertContact(Request $request)
    {
        $data = ['nombre' => $request['nombre'],
            'correo' => $request['correo'],
            'email' => $request['email'],
            'descripcion' => $request['descripcion'],
            'celular' => $request['celular']
        ];
        if (DB::table('contacts')->where('correo', $request['correo'])->exists()) {
            return redirect()->route('formulario')->with('error', 'El correo que ingreso ya está registrado');
        } else {
            $contact = Contact::create($data);
            return redirect()->back()->with('success', 'se ha guardado con éxito, pronto nos pondremos en contacto con tigo');
        }
    }
    public function showFormulario()
    {
        return View('form');
    }
    public function editContact($id)
    {
        $contact = Contact::find($id);
        if (is_null($contact)) {
            return ['status' => false, 'message' => 'este id no existe'];
        } else {
            $contact->descripcion = 'prueba descripcion';
            $contact->save();
            return ['status' => true, 'message' => 'Se edito el contacto correctamente'];
        }
    }
    public function deleteContact($id)
    {
        $contact = Contact::find($id);
        if (is_null($contact)) {
            return ['status' => false, 'message' => 'este id no existe'];
        } else {
            $contact->delete();
            return ['status' => true, 'message' => 'Se elimino el contacto correctamente'];
        }
    }
}
