<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'intervaloEntrega',
        'correo',
        'telefono',
        'created_at',
        'updated_at',
    ];
}
