/*
Navicat MySQL Data Transfer

Source Server         : localhost Mysql
Source Server Version : 80021
Source Host           : localhost:3306
Source Database       : osaio

Target Server Type    : MYSQL
Target Server Version : 80021
File Encoding         : 65001

Date: 2022-10-19 20:45:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `clients`
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES ('1', 'diana1@gmail.com', 'Diana Carolina', 'Acosta Duarte', '2022-09-11 00:58:00', '2022-09-11 00:58:53');
INSERT INTO `clients` VALUES ('3', 'diana2@gmail.com', 'Carolina', 'Acosta', '2022-09-11 00:59:26', '2022-09-11 00:59:26');
INSERT INTO `clients` VALUES ('5', 'profe@sena.edu.com.co', 'profe', 'profe', '2022-09-11 03:36:40', '2022-09-11 03:36:40');

-- ----------------------------
-- Table structure for `contacts`
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `correo` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `celular` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES ('1', 'Diana', 'diana@gmail.com', 'prueba descripcion', null, '2022-09-20 04:54:02', '2022-09-28 00:23:41');

-- ----------------------------
-- Table structure for `data_rows`
-- ----------------------------
DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_rows
-- ----------------------------
INSERT INTO `data_rows` VALUES ('1', '1', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('2', '1', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('3', '1', 'email', 'text', 'Email', '1', '1', '1', '1', '1', '1', null, '3');
INSERT INTO `data_rows` VALUES ('4', '1', 'password', 'password', 'Password', '1', '0', '0', '1', '1', '0', null, '4');
INSERT INTO `data_rows` VALUES ('5', '1', 'remember_token', 'text', 'Remember Token', '0', '0', '0', '0', '0', '0', null, '5');
INSERT INTO `data_rows` VALUES ('6', '1', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '0', '0', '0', null, '6');
INSERT INTO `data_rows` VALUES ('7', '1', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '7');
INSERT INTO `data_rows` VALUES ('8', '1', 'avatar', 'image', 'Avatar', '0', '1', '1', '1', '1', '1', null, '8');
INSERT INTO `data_rows` VALUES ('9', '1', 'user_belongsto_role_relationship', 'relationship', 'Role', '0', '1', '1', '1', '1', '0', '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', '10');
INSERT INTO `data_rows` VALUES ('10', '1', 'user_belongstomany_role_relationship', 'relationship', 'voyager::seeders.data_rows.roles', '0', '1', '1', '1', '1', '0', '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', '11');
INSERT INTO `data_rows` VALUES ('11', '1', 'settings', 'hidden', 'Settings', '0', '0', '0', '0', '0', '0', null, '12');
INSERT INTO `data_rows` VALUES ('12', '2', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('13', '2', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('14', '2', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('15', '2', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('16', '3', 'id', 'number', 'ID', '1', '0', '0', '0', '0', '0', null, '1');
INSERT INTO `data_rows` VALUES ('17', '3', 'name', 'text', 'Name', '1', '1', '1', '1', '1', '1', null, '2');
INSERT INTO `data_rows` VALUES ('18', '3', 'created_at', 'timestamp', 'Created At', '0', '0', '0', '0', '0', '0', null, '3');
INSERT INTO `data_rows` VALUES ('19', '3', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', null, '4');
INSERT INTO `data_rows` VALUES ('20', '3', 'display_name', 'text', 'Display Name', '1', '1', '1', '1', '1', '1', null, '5');
INSERT INTO `data_rows` VALUES ('21', '1', 'role_id', 'text', 'Role', '1', '1', '1', '1', '1', '1', null, '9');
INSERT INTO `data_rows` VALUES ('22', '4', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('23', '4', 'correo', 'text', 'Correo', '1', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('24', '4', 'nombre', 'text', 'Nombre', '1', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('25', '4', 'apellido', 'text', 'Apellido', '1', '1', '1', '1', '1', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('26', '4', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '1', '0', '1', '{}', '5');
INSERT INTO `data_rows` VALUES ('27', '4', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '6');
INSERT INTO `data_rows` VALUES ('28', '5', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('29', '5', 'sueldo', 'number', 'Sueldo', '1', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('30', '5', 'nombre', 'text', 'Nombre', '1', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('31', '5', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '1', '0', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('32', '5', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '5');
INSERT INTO `data_rows` VALUES ('33', '6', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('34', '6', 'nombre', 'text', 'Nombre', '1', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('35', '6', 'descripcion', 'text_area', 'Descripcion', '0', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('36', '6', 'cantidad', 'number', 'Cantidad', '0', '1', '1', '1', '1', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('37', '6', 'precioVenta', 'number', 'PrecioVenta', '1', '1', '1', '1', '1', '1', '{}', '5');
INSERT INTO `data_rows` VALUES ('38', '6', 'precioReal', 'number', 'PrecioReal', '0', '1', '1', '1', '1', '1', '{}', '6');
INSERT INTO `data_rows` VALUES ('39', '6', 'marca', 'text', 'Marca', '1', '1', '1', '1', '1', '1', '{}', '7');
INSERT INTO `data_rows` VALUES ('40', '6', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '1', '0', '1', '{}', '8');
INSERT INTO `data_rows` VALUES ('41', '6', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '9');
INSERT INTO `data_rows` VALUES ('42', '7', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('43', '7', 'nombre', 'text', 'Nombre', '1', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('44', '7', 'intervaloEntrega', 'number', 'Intervalo de entrega (En días)', '1', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('45', '7', 'correo', 'text', 'Correo', '1', '1', '1', '1', '1', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('46', '7', 'telefono', 'text', 'Telefono', '1', '1', '1', '1', '1', '1', '{}', '5');
INSERT INTO `data_rows` VALUES ('47', '7', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '1', '0', '1', '{}', '6');
INSERT INTO `data_rows` VALUES ('48', '7', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '7');
INSERT INTO `data_rows` VALUES ('49', '8', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('50', '8', 'created_at', 'timestamp', 'Created At', '1', '1', '1', '1', '0', '1', '{}', '5');
INSERT INTO `data_rows` VALUES ('51', '8', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '6');
INSERT INTO `data_rows` VALUES ('52', '8', 'receipt_hasmany_client_relationship', 'relationship', 'Productos', '1', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Models\\\\Product\",\"table\":\"products\",\"type\":\"belongsTo\",\"column\":\"producto_id\",\"key\":\"id\",\"label\":\"nombre\",\"pivot_table\":\"clients\",\"pivot\":\"0\",\"taggable\":\"0\"}', '2');
INSERT INTO `data_rows` VALUES ('53', '8', 'receipt_hasone_seller_relationship', 'relationship', 'Vendedor', '1', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Models\\\\Seller\",\"table\":\"sellers\",\"type\":\"belongsTo\",\"column\":\"vendedor_id\",\"key\":\"id\",\"label\":\"nombre\",\"pivot_table\":\"clients\",\"pivot\":\"0\",\"taggable\":\"0\"}', '3');
INSERT INTO `data_rows` VALUES ('54', '8', 'receipt_hasone_client_relationship', 'relationship', 'Cliente', '1', '1', '1', '1', '1', '1', '{\"model\":\"App\\\\Models\\\\Client\",\"table\":\"clients\",\"type\":\"belongsTo\",\"column\":\"cliente_id\",\"key\":\"id\",\"label\":\"nombre\",\"pivot_table\":\"clients\",\"pivot\":\"0\",\"taggable\":\"0\"}', '4');
INSERT INTO `data_rows` VALUES ('55', '8', 'producto_id', 'text', 'Producto Id', '1', '1', '1', '1', '1', '1', '{}', '7');
INSERT INTO `data_rows` VALUES ('56', '8', 'vendedor_id', 'text', 'Vendedor Id', '1', '1', '1', '1', '1', '1', '{}', '8');
INSERT INTO `data_rows` VALUES ('57', '8', 'cliente_id', 'text', 'Cliente Id', '1', '1', '1', '1', '1', '1', '{}', '9');
INSERT INTO `data_rows` VALUES ('58', '10', 'id', 'text', 'Id', '1', '0', '0', '0', '0', '0', '{}', '1');
INSERT INTO `data_rows` VALUES ('59', '10', 'nombre', 'text', 'Nombre', '1', '1', '1', '1', '1', '1', '{}', '2');
INSERT INTO `data_rows` VALUES ('60', '10', 'correo', 'text', 'Correo', '1', '1', '1', '1', '1', '1', '{}', '3');
INSERT INTO `data_rows` VALUES ('61', '10', 'descripcion', 'text_area', 'Descripcion', '0', '1', '1', '1', '1', '1', '{}', '4');
INSERT INTO `data_rows` VALUES ('62', '10', 'celular', 'text', 'Celular', '0', '1', '1', '1', '1', '1', '{}', '5');
INSERT INTO `data_rows` VALUES ('63', '10', 'created_at', 'timestamp', 'Created At', '0', '1', '1', '1', '0', '1', '{}', '6');
INSERT INTO `data_rows` VALUES ('64', '10', 'updated_at', 'timestamp', 'Updated At', '0', '0', '0', '0', '0', '0', '{}', '7');

-- ----------------------------
-- Table structure for `data_types`
-- ----------------------------
DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of data_types
-- ----------------------------
INSERT INTO `data_types` VALUES ('1', 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', '1', '0', null, '2022-09-11 00:18:02', '2022-09-11 00:18:02');
INSERT INTO `data_types` VALUES ('2', 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', null, '', '', '1', '0', null, '2022-09-11 00:18:02', '2022-09-11 00:18:02');
INSERT INTO `data_types` VALUES ('3', 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', null, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', '1', '0', null, '2022-09-11 00:18:03', '2022-09-11 00:18:03');
INSERT INTO `data_types` VALUES ('4', 'clients', 'clients', 'Cliente', 'Clientes', 'voyager-people', 'App\\Models\\Client', null, null, 'Este módulo es para agregar editar consultar o eliminar información del cliente', '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2022-09-11 00:52:56', '2022-09-11 00:52:56');
INSERT INTO `data_types` VALUES ('5', 'sellers', 'sellers', 'Vendedor', 'Vendedores', 'voyager-bar-chart', 'App\\Models\\Seller', null, null, 'Este modulo sirve para poder editar, consultar, borrar o agregar vendedores', '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2022-09-11 01:39:32', '2022-09-11 01:39:32');
INSERT INTO `data_types` VALUES ('6', 'products', 'products', 'Producto', 'Productos', 'voyager-basket', 'App\\Models\\Product', null, null, 'Este Modulo sirve para ver, agregar, modificar o eliminar información del producto', '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2022-09-11 02:00:29', '2022-09-11 02:00:29');
INSERT INTO `data_types` VALUES ('7', 'suppliers', 'suppliers', 'Proveedor', 'Proveedores', 'voyager-truck', 'App\\Models\\Supplier', null, null, 'En este modulo se pueden agregar, actualizar, ver o eliminar proveedores', '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2022-09-11 02:14:04', '2022-09-11 02:14:04');
INSERT INTO `data_types` VALUES ('8', 'receipts', 'receipts', 'Factura', 'Facturas', 'voyager-receipt', 'App\\Models\\Receipt', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2022-09-11 02:27:46', '2022-09-11 02:58:28');
INSERT INTO `data_types` VALUES ('10', 'contacts', 'contacts', 'Contacto', 'Contactos', 'voyager-window-list', 'App\\Models\\Contact', null, null, null, '1', '0', '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2022-09-20 01:43:55', '2022-09-20 01:43:55');

-- ----------------------------
-- Table structure for `failed_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'admin', '2022-09-11 00:18:12', '2022-09-11 00:18:12');

-- ----------------------------
-- Table structure for `menu_items`
-- ----------------------------
DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `order` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of menu_items
-- ----------------------------
INSERT INTO `menu_items` VALUES ('1', '1', 'Dashboard', '', '_self', 'voyager-boat', null, null, '1', '2022-09-11 00:18:12', '2022-09-11 00:18:12', 'voyager.dashboard', null);
INSERT INTO `menu_items` VALUES ('2', '1', 'Media', '', '_self', 'voyager-images', null, null, '5', '2022-09-11 00:18:12', '2022-09-11 00:18:12', 'voyager.media.index', null);
INSERT INTO `menu_items` VALUES ('3', '1', 'Users', '', '_self', 'voyager-person', null, null, '3', '2022-09-11 00:18:13', '2022-09-11 00:18:13', 'voyager.users.index', null);
INSERT INTO `menu_items` VALUES ('4', '1', 'Roles', '', '_self', 'voyager-lock', null, null, '2', '2022-09-11 00:18:13', '2022-09-11 00:18:13', 'voyager.roles.index', null);
INSERT INTO `menu_items` VALUES ('5', '1', 'Tools', '', '_self', 'voyager-tools', null, null, '9', '2022-09-11 00:18:14', '2022-09-11 00:18:14', null, null);
INSERT INTO `menu_items` VALUES ('6', '1', 'Menu Builder', '', '_self', 'voyager-list', null, '5', '10', '2022-09-11 00:18:14', '2022-09-11 00:18:14', 'voyager.menus.index', null);
INSERT INTO `menu_items` VALUES ('7', '1', 'Database', '', '_self', 'voyager-data', null, '5', '11', '2022-09-11 00:18:15', '2022-09-11 00:18:15', 'voyager.database.index', null);
INSERT INTO `menu_items` VALUES ('8', '1', 'Compass', '', '_self', 'voyager-compass', null, '5', '12', '2022-09-11 00:18:15', '2022-09-11 00:18:15', 'voyager.compass.index', null);
INSERT INTO `menu_items` VALUES ('9', '1', 'BREAD', '', '_self', 'voyager-bread', null, '5', '13', '2022-09-11 00:18:15', '2022-09-11 00:18:15', 'voyager.bread.index', null);
INSERT INTO `menu_items` VALUES ('10', '1', 'Settings', '', '_self', 'voyager-settings', null, null, '14', '2022-09-11 00:18:15', '2022-09-11 00:18:15', 'voyager.settings.index', null);
INSERT INTO `menu_items` VALUES ('11', '1', 'Clientes', '', '_self', 'voyager-people', null, null, '15', '2022-09-11 00:52:56', '2022-09-11 00:52:56', 'voyager.clients.index', null);
INSERT INTO `menu_items` VALUES ('12', '1', 'Vendedores', '', '_self', 'voyager-bar-chart', null, null, '16', '2022-09-11 01:39:32', '2022-09-11 01:39:32', 'voyager.sellers.index', null);
INSERT INTO `menu_items` VALUES ('13', '1', 'Productos', '', '_self', 'voyager-basket', null, null, '17', '2022-09-11 02:00:29', '2022-09-11 02:00:29', 'voyager.products.index', null);
INSERT INTO `menu_items` VALUES ('14', '1', 'Proveedores', '', '_self', 'voyager-truck', null, null, '18', '2022-09-11 02:14:04', '2022-09-11 02:14:04', 'voyager.suppliers.index', null);
INSERT INTO `menu_items` VALUES ('15', '1', 'Facturas', '', '_self', 'voyager-receipt', null, null, '19', '2022-09-11 02:27:46', '2022-09-11 02:27:46', 'voyager.receipts.index', null);
INSERT INTO `menu_items` VALUES ('16', '1', 'Contactos', '', '_self', 'voyager-window-list', null, null, '20', '2022-09-20 01:43:56', '2022-09-20 01:43:56', 'voyager.contacts.index', null);

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2016_01_01_000000_add_voyager_user_fields', '1');
INSERT INTO `migrations` VALUES ('4', '2016_01_01_000000_create_data_types_table', '1');
INSERT INTO `migrations` VALUES ('5', '2016_05_19_173453_create_menu_table', '1');
INSERT INTO `migrations` VALUES ('6', '2016_10_21_190000_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('7', '2016_10_21_190000_create_settings_table', '1');
INSERT INTO `migrations` VALUES ('8', '2016_11_30_135954_create_permission_table', '1');
INSERT INTO `migrations` VALUES ('9', '2016_11_30_141208_create_permission_role_table', '1');
INSERT INTO `migrations` VALUES ('10', '2016_12_26_201236_data_types__add__server_side', '1');
INSERT INTO `migrations` VALUES ('11', '2017_01_13_000000_add_route_to_menu_items_table', '1');
INSERT INTO `migrations` VALUES ('12', '2017_01_14_005015_create_translations_table', '1');
INSERT INTO `migrations` VALUES ('13', '2017_01_15_000000_make_table_name_nullable_in_permissions_table', '1');
INSERT INTO `migrations` VALUES ('14', '2017_03_06_000000_add_controller_to_data_types_table', '1');
INSERT INTO `migrations` VALUES ('15', '2017_04_21_000000_add_order_to_data_rows_table', '1');
INSERT INTO `migrations` VALUES ('16', '2017_07_05_210000_add_policyname_to_data_types_table', '1');
INSERT INTO `migrations` VALUES ('17', '2017_08_05_000000_add_group_to_settings_table', '1');
INSERT INTO `migrations` VALUES ('18', '2017_11_26_013050_add_user_role_relationship', '1');
INSERT INTO `migrations` VALUES ('19', '2017_11_26_015000_create_user_roles_table', '1');
INSERT INTO `migrations` VALUES ('20', '2018_03_11_000000_add_user_settings', '1');
INSERT INTO `migrations` VALUES ('21', '2018_03_14_000000_add_details_to_data_types_table', '1');
INSERT INTO `migrations` VALUES ('22', '2018_03_16_000000_make_settings_value_nullable', '1');
INSERT INTO `migrations` VALUES ('23', '2019_08_19_000000_create_failed_jobs_table', '1');
INSERT INTO `migrations` VALUES ('24', '2019_12_14_000001_create_personal_access_tokens_table', '1');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES ('1', 'browse_admin', null, '2022-09-11 00:18:16', '2022-09-11 00:18:16');
INSERT INTO `permissions` VALUES ('2', 'browse_bread', null, '2022-09-11 00:18:16', '2022-09-11 00:18:16');
INSERT INTO `permissions` VALUES ('3', 'browse_database', null, '2022-09-11 00:18:16', '2022-09-11 00:18:16');
INSERT INTO `permissions` VALUES ('4', 'browse_media', null, '2022-09-11 00:18:16', '2022-09-11 00:18:16');
INSERT INTO `permissions` VALUES ('5', 'browse_compass', null, '2022-09-11 00:18:16', '2022-09-11 00:18:16');
INSERT INTO `permissions` VALUES ('6', 'browse_menus', 'menus', '2022-09-11 00:18:17', '2022-09-11 00:18:17');
INSERT INTO `permissions` VALUES ('7', 'read_menus', 'menus', '2022-09-11 00:18:17', '2022-09-11 00:18:17');
INSERT INTO `permissions` VALUES ('8', 'edit_menus', 'menus', '2022-09-11 00:18:18', '2022-09-11 00:18:18');
INSERT INTO `permissions` VALUES ('9', 'add_menus', 'menus', '2022-09-11 00:18:18', '2022-09-11 00:18:18');
INSERT INTO `permissions` VALUES ('10', 'delete_menus', 'menus', '2022-09-11 00:18:19', '2022-09-11 00:18:19');
INSERT INTO `permissions` VALUES ('11', 'browse_roles', 'roles', '2022-09-11 00:18:20', '2022-09-11 00:18:20');
INSERT INTO `permissions` VALUES ('12', 'read_roles', 'roles', '2022-09-11 00:18:21', '2022-09-11 00:18:21');
INSERT INTO `permissions` VALUES ('13', 'edit_roles', 'roles', '2022-09-11 00:18:22', '2022-09-11 00:18:22');
INSERT INTO `permissions` VALUES ('14', 'add_roles', 'roles', '2022-09-11 00:18:22', '2022-09-11 00:18:22');
INSERT INTO `permissions` VALUES ('15', 'delete_roles', 'roles', '2022-09-11 00:18:22', '2022-09-11 00:18:22');
INSERT INTO `permissions` VALUES ('16', 'browse_users', 'users', '2022-09-11 00:18:22', '2022-09-11 00:18:22');
INSERT INTO `permissions` VALUES ('17', 'read_users', 'users', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('18', 'edit_users', 'users', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('19', 'add_users', 'users', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('20', 'delete_users', 'users', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('21', 'browse_settings', 'settings', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('22', 'read_settings', 'settings', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('23', 'edit_settings', 'settings', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('24', 'add_settings', 'settings', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('25', 'delete_settings', 'settings', '2022-09-11 00:18:23', '2022-09-11 00:18:23');
INSERT INTO `permissions` VALUES ('26', 'browse_clients', 'clients', '2022-09-11 00:52:56', '2022-09-11 00:52:56');
INSERT INTO `permissions` VALUES ('27', 'read_clients', 'clients', '2022-09-11 00:52:56', '2022-09-11 00:52:56');
INSERT INTO `permissions` VALUES ('28', 'edit_clients', 'clients', '2022-09-11 00:52:56', '2022-09-11 00:52:56');
INSERT INTO `permissions` VALUES ('29', 'add_clients', 'clients', '2022-09-11 00:52:56', '2022-09-11 00:52:56');
INSERT INTO `permissions` VALUES ('30', 'delete_clients', 'clients', '2022-09-11 00:52:56', '2022-09-11 00:52:56');
INSERT INTO `permissions` VALUES ('31', 'browse_sellers', 'sellers', '2022-09-11 01:39:32', '2022-09-11 01:39:32');
INSERT INTO `permissions` VALUES ('32', 'read_sellers', 'sellers', '2022-09-11 01:39:32', '2022-09-11 01:39:32');
INSERT INTO `permissions` VALUES ('33', 'edit_sellers', 'sellers', '2022-09-11 01:39:32', '2022-09-11 01:39:32');
INSERT INTO `permissions` VALUES ('34', 'add_sellers', 'sellers', '2022-09-11 01:39:32', '2022-09-11 01:39:32');
INSERT INTO `permissions` VALUES ('35', 'delete_sellers', 'sellers', '2022-09-11 01:39:32', '2022-09-11 01:39:32');
INSERT INTO `permissions` VALUES ('36', 'browse_products', 'products', '2022-09-11 02:00:29', '2022-09-11 02:00:29');
INSERT INTO `permissions` VALUES ('37', 'read_products', 'products', '2022-09-11 02:00:29', '2022-09-11 02:00:29');
INSERT INTO `permissions` VALUES ('38', 'edit_products', 'products', '2022-09-11 02:00:29', '2022-09-11 02:00:29');
INSERT INTO `permissions` VALUES ('39', 'add_products', 'products', '2022-09-11 02:00:29', '2022-09-11 02:00:29');
INSERT INTO `permissions` VALUES ('40', 'delete_products', 'products', '2022-09-11 02:00:29', '2022-09-11 02:00:29');
INSERT INTO `permissions` VALUES ('41', 'browse_suppliers', 'suppliers', '2022-09-11 02:14:04', '2022-09-11 02:14:04');
INSERT INTO `permissions` VALUES ('42', 'read_suppliers', 'suppliers', '2022-09-11 02:14:04', '2022-09-11 02:14:04');
INSERT INTO `permissions` VALUES ('43', 'edit_suppliers', 'suppliers', '2022-09-11 02:14:04', '2022-09-11 02:14:04');
INSERT INTO `permissions` VALUES ('44', 'add_suppliers', 'suppliers', '2022-09-11 02:14:04', '2022-09-11 02:14:04');
INSERT INTO `permissions` VALUES ('45', 'delete_suppliers', 'suppliers', '2022-09-11 02:14:04', '2022-09-11 02:14:04');
INSERT INTO `permissions` VALUES ('46', 'browse_receipts', 'receipts', '2022-09-11 02:27:46', '2022-09-11 02:27:46');
INSERT INTO `permissions` VALUES ('47', 'read_receipts', 'receipts', '2022-09-11 02:27:46', '2022-09-11 02:27:46');
INSERT INTO `permissions` VALUES ('48', 'edit_receipts', 'receipts', '2022-09-11 02:27:46', '2022-09-11 02:27:46');
INSERT INTO `permissions` VALUES ('49', 'add_receipts', 'receipts', '2022-09-11 02:27:46', '2022-09-11 02:27:46');
INSERT INTO `permissions` VALUES ('50', 'delete_receipts', 'receipts', '2022-09-11 02:27:46', '2022-09-11 02:27:46');
INSERT INTO `permissions` VALUES ('51', 'browse_contacts', 'contacts', '2022-09-20 01:43:55', '2022-09-20 01:43:55');
INSERT INTO `permissions` VALUES ('52', 'read_contacts', 'contacts', '2022-09-20 01:43:55', '2022-09-20 01:43:55');
INSERT INTO `permissions` VALUES ('53', 'edit_contacts', 'contacts', '2022-09-20 01:43:55', '2022-09-20 01:43:55');
INSERT INTO `permissions` VALUES ('54', 'add_contacts', 'contacts', '2022-09-20 01:43:55', '2022-09-20 01:43:55');
INSERT INTO `permissions` VALUES ('55', 'delete_contacts', 'contacts', '2022-09-20 01:43:55', '2022-09-20 01:43:55');

-- ----------------------------
-- Table structure for `permission_role`
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role` (
  `permission_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES ('1', '1');
INSERT INTO `permission_role` VALUES ('1', '2');
INSERT INTO `permission_role` VALUES ('2', '1');
INSERT INTO `permission_role` VALUES ('3', '1');
INSERT INTO `permission_role` VALUES ('4', '1');
INSERT INTO `permission_role` VALUES ('5', '1');
INSERT INTO `permission_role` VALUES ('6', '1');
INSERT INTO `permission_role` VALUES ('7', '1');
INSERT INTO `permission_role` VALUES ('8', '1');
INSERT INTO `permission_role` VALUES ('9', '1');
INSERT INTO `permission_role` VALUES ('10', '1');
INSERT INTO `permission_role` VALUES ('11', '1');
INSERT INTO `permission_role` VALUES ('12', '1');
INSERT INTO `permission_role` VALUES ('13', '1');
INSERT INTO `permission_role` VALUES ('14', '1');
INSERT INTO `permission_role` VALUES ('15', '1');
INSERT INTO `permission_role` VALUES ('16', '1');
INSERT INTO `permission_role` VALUES ('17', '1');
INSERT INTO `permission_role` VALUES ('18', '1');
INSERT INTO `permission_role` VALUES ('19', '1');
INSERT INTO `permission_role` VALUES ('20', '1');
INSERT INTO `permission_role` VALUES ('21', '1');
INSERT INTO `permission_role` VALUES ('22', '1');
INSERT INTO `permission_role` VALUES ('23', '1');
INSERT INTO `permission_role` VALUES ('24', '1');
INSERT INTO `permission_role` VALUES ('25', '1');
INSERT INTO `permission_role` VALUES ('26', '1');
INSERT INTO `permission_role` VALUES ('26', '2');
INSERT INTO `permission_role` VALUES ('27', '1');
INSERT INTO `permission_role` VALUES ('27', '2');
INSERT INTO `permission_role` VALUES ('28', '1');
INSERT INTO `permission_role` VALUES ('28', '2');
INSERT INTO `permission_role` VALUES ('29', '1');
INSERT INTO `permission_role` VALUES ('29', '2');
INSERT INTO `permission_role` VALUES ('30', '1');
INSERT INTO `permission_role` VALUES ('30', '2');
INSERT INTO `permission_role` VALUES ('31', '1');
INSERT INTO `permission_role` VALUES ('31', '2');
INSERT INTO `permission_role` VALUES ('32', '1');
INSERT INTO `permission_role` VALUES ('32', '2');
INSERT INTO `permission_role` VALUES ('33', '1');
INSERT INTO `permission_role` VALUES ('33', '2');
INSERT INTO `permission_role` VALUES ('34', '1');
INSERT INTO `permission_role` VALUES ('34', '2');
INSERT INTO `permission_role` VALUES ('35', '1');
INSERT INTO `permission_role` VALUES ('35', '2');
INSERT INTO `permission_role` VALUES ('36', '1');
INSERT INTO `permission_role` VALUES ('36', '2');
INSERT INTO `permission_role` VALUES ('37', '1');
INSERT INTO `permission_role` VALUES ('37', '2');
INSERT INTO `permission_role` VALUES ('38', '1');
INSERT INTO `permission_role` VALUES ('38', '2');
INSERT INTO `permission_role` VALUES ('39', '1');
INSERT INTO `permission_role` VALUES ('39', '2');
INSERT INTO `permission_role` VALUES ('40', '1');
INSERT INTO `permission_role` VALUES ('40', '2');
INSERT INTO `permission_role` VALUES ('41', '1');
INSERT INTO `permission_role` VALUES ('41', '2');
INSERT INTO `permission_role` VALUES ('42', '1');
INSERT INTO `permission_role` VALUES ('42', '2');
INSERT INTO `permission_role` VALUES ('43', '1');
INSERT INTO `permission_role` VALUES ('43', '2');
INSERT INTO `permission_role` VALUES ('44', '1');
INSERT INTO `permission_role` VALUES ('44', '2');
INSERT INTO `permission_role` VALUES ('45', '1');
INSERT INTO `permission_role` VALUES ('45', '2');
INSERT INTO `permission_role` VALUES ('46', '1');
INSERT INTO `permission_role` VALUES ('46', '2');
INSERT INTO `permission_role` VALUES ('47', '1');
INSERT INTO `permission_role` VALUES ('47', '2');
INSERT INTO `permission_role` VALUES ('48', '1');
INSERT INTO `permission_role` VALUES ('48', '2');
INSERT INTO `permission_role` VALUES ('49', '1');
INSERT INTO `permission_role` VALUES ('49', '2');
INSERT INTO `permission_role` VALUES ('50', '1');
INSERT INTO `permission_role` VALUES ('50', '2');
INSERT INTO `permission_role` VALUES ('51', '1');
INSERT INTO `permission_role` VALUES ('51', '2');
INSERT INTO `permission_role` VALUES ('52', '1');
INSERT INTO `permission_role` VALUES ('52', '2');
INSERT INTO `permission_role` VALUES ('53', '1');
INSERT INTO `permission_role` VALUES ('53', '2');
INSERT INTO `permission_role` VALUES ('54', '1');
INSERT INTO `permission_role` VALUES ('54', '2');
INSERT INTO `permission_role` VALUES ('55', '1');
INSERT INTO `permission_role` VALUES ('55', '2');

-- ----------------------------
-- Table structure for `personal_access_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci,
  `cantidad` int DEFAULT NULL,
  `precioVenta` int NOT NULL,
  `precioReal` int DEFAULT NULL,
  `marca` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', 'Chocorramo', 'Esponjoso ponque cubierto de chocolate', '200', '1700', '1200', 'Ramo', '2022-09-11 02:03:56', '2022-09-11 02:03:56');
INSERT INTO `products` VALUES ('2', 'Leche', 'producto lacteo', '5', '5000', '4500', 'Alqueria', '2022-09-11 03:31:57', '2022-09-11 03:31:57');
INSERT INTO `products` VALUES ('3', 'Pasta', 'deliciosa pasta de harina', '32', '6950', '5000', 'Doria', '2022-09-21 22:27:44', '2022-09-21 22:27:44');
INSERT INTO `products` VALUES ('4', 'galletas', 'saltin noel taco', '100', '8140', '6000', 'saltin noel', '2022-09-21 22:49:53', '2022-09-21 22:49:53');

-- ----------------------------
-- Table structure for `receipts`
-- ----------------------------
DROP TABLE IF EXISTS `receipts`;
CREATE TABLE `receipts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `producto_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendedor_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cliente_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of receipts
-- ----------------------------
INSERT INTO `receipts` VALUES ('1', '2022-09-11 02:58:00', '2022-09-11 03:33:21', '1', '1', '3');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin', 'Administrator', '2022-09-11 00:18:16', '2022-09-11 00:18:16');
INSERT INTO `roles` VALUES ('2', 'user', 'Normal User', '2022-09-11 00:18:16', '2022-09-11 00:18:16');

-- ----------------------------
-- Table structure for `sellers`
-- ----------------------------
DROP TABLE IF EXISTS `sellers`;
CREATE TABLE `sellers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sueldo` int NOT NULL,
  `nombre` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sellers
-- ----------------------------
INSERT INTO `sellers` VALUES ('1', '1000000', 'Diana Carolina Acosta', '2022-09-11 01:40:29', '2022-09-11 01:40:29');

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('1', 'site.title', 'Site Title', 'Osaio', '', 'text', '1', 'Site');
INSERT INTO `settings` VALUES ('2', 'site.description', 'Site Description', 'Administrador de productos', '', 'text', '2', 'Site');
INSERT INTO `settings` VALUES ('3', 'site.logo', 'Site Logo', '', '', 'image', '3', 'Site');
INSERT INTO `settings` VALUES ('4', 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', null, '', 'text', '4', 'Site');
INSERT INTO `settings` VALUES ('5', 'admin.bg_image', 'Admin Background Image', '', '', 'image', '5', 'Admin');
INSERT INTO `settings` VALUES ('6', 'admin.title', 'Admin Title', 'Osaio', '', 'text', '1', 'Admin');
INSERT INTO `settings` VALUES ('7', 'admin.description', 'Admin Description', 'Bienvenido a Osaio. Admin de gestión de productos', '', 'text', '2', 'Admin');
INSERT INTO `settings` VALUES ('8', 'admin.loader', 'Admin Loader', '', '', 'image', '3', 'Admin');
INSERT INTO `settings` VALUES ('9', 'admin.icon_image', 'Admin Icon Image', '', '', 'image', '4', 'Admin');
INSERT INTO `settings` VALUES ('10', 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', null, '', 'text', '1', 'Admin');

-- ----------------------------
-- Table structure for `suppliers`
-- ----------------------------
DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intervaloEntrega` int NOT NULL,
  `correo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of suppliers
-- ----------------------------
INSERT INTO `suppliers` VALUES ('1', 'Ramo', '7', 'ramo@gmail.com', '3105678932', '2022-09-11 02:15:35', '2022-09-11 02:15:35');

-- ----------------------------
-- Table structure for `translations`
-- ----------------------------
DROP TABLE IF EXISTS `translations`;
CREATE TABLE `translations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of translations
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', '1', 'Diana Acosta', 'dianac.acostad@gmail.com', 'users/default.png', null, '$2y$10$E/fuevySS4vTzAyPpinlDOZrR.JejwS8rzLV2pYE0QKNrnNjp.yKi', null, null, '2022-09-11 00:23:16', '2022-09-11 00:23:16');
INSERT INTO `users` VALUES ('2', '2', 'Usuario normal', 'user@gmail.com', 'users/default.png', null, '$2y$10$kshh.mGiq55mhP/lSCjeY.yn6oQIZ6XCq3tCEpJIF9JiILq.c8d2i', null, '{\"locale\":\"es\"}', '2022-09-11 03:02:16', '2022-09-11 03:02:16');

-- ----------------------------
-- Table structure for `user_roles`
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `user_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of user_roles
-- ----------------------------

-- ----------------------------
-- Procedure structure for `Cantidades`
-- ----------------------------
DROP PROCEDURE IF EXISTS `Cantidades`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Cantidades`(IN `number` int)
BEGIN
	SELECT * FROM products where cantidad > number;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `Ganancia`
-- ----------------------------
DROP FUNCTION IF EXISTS `Ganancia`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `Ganancia`() RETURNS int
BEGIN
	DECLARE salida INT;
	SET salida = (SELECT MAX(precioVenta - precioReal) as ganancia FROM products);
	RETURN salida;
END
;;
DELIMITER ;
DROP TRIGGER IF EXISTS `ActualizacionPrecioVenta`;
DELIMITER ;;
CREATE TRIGGER `ActualizacionPrecioVenta` BEFORE INSERT ON `products` FOR EACH ROW BEGIN
      SET NEW.precioVenta = NEW.precioVenta  + NEW.precioReal * 0.19;
END
;;
DELIMITER ;
